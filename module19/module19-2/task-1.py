small_storage = {
    'гвозди': 5000,
    'шурупы': 3040,
    'саморезы': 2000
}

big_storage = {
    'доски': 1000,
    'балки': 150,
    'рейки': 600
}

big_storage.update(small_storage)

while True:
    tovar = input('Введите название товара: ')
    if tovar in big_storage:
        print(big_storage.get(tovar))
    else:
        print('Такого товара нет:', big_storage.get(tovar))