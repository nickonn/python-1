incomes = {
    'apple': 5600.20,
    'orange': 3500.45,
    'banana': 5000.00,
    'bergamot': 3700.56,
    'durian': 5987.23,
    'grapefruit': 300.40,
    'peach': 10000.50,
    'pear': 1020.00,
    'persimmon': 310.00,
}
total_sum = 0
for i in incomes.values():
    total_sum += i

min_tovar = dict()

for k, v in incomes.items():
    if v == min(incomes.values()):
        min_tovar = k

incomes.pop(min_tovar)

print('Общий доход за год составил:', total_sum)
print('Самый маленький доход у', min_tovar, 'Он составляет', min(incomes.values()))
print('Итоговый словарь: ', incomes)
