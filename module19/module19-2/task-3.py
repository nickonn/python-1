def histogram(string):
    sym_dict = dict()
    for i in string:
        if i in sym_dict:
            sym_dict[i] += 1
        else:
            sym_dict[i] = 1
    return sym_dict

text = input('Введите текст: ')
hist = histogram(text)
for i in sorted(hist.keys()):
    print(i, ":", hist[i])

print('Максимальная частота: ', max(hist.values()))