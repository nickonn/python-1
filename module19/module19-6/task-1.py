violator_songs = {
    'World in My Eyes': 4.86,
    'Sweetest Perfection': 4.43,
    'Personal Jesus': 4.56,
    'Halo': 4.9,
    'Waiting for the Night': 6.07,
    'Enjoy the Silence': 4.20,
    'Policy of Truth': 4.76,
    'Blue Dress': 4.29,
    'Clean': 5.83
}

kol_song = int(input('Сколько песен выбрать? '))
time_song = 0

for i in range(kol_song):
    name_song = input('Название первой песни: ')
    for k, v in violator_songs.items():
        if k == name_song:
            time_song += v

print('Общее время звучания песен: ', round(time_song, 2))