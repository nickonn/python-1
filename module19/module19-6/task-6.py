kol = int(input('Введите количество пар слов: '))

text_dict = dict()
for i in range(kol):
    text = input(f'\n {i+1} пара: ').lower().split('-')
    text_dict[text[0].strip()] = text[1].strip()
    text_dict[text[1].strip()] = text[0].strip()

for i in text_dict.keys():
    print(i, ":", text_dict[i])

while True:
    word = input('\nВведите слово: ').lower().strip()
    if word == 'end':
        break
    elif word in text_dict:
        print('Синоним:', text_dict[word])
    else:
        print('Такого слова в словаре нет.')