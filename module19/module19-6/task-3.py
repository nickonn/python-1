data = {
    "address": "0x544444444444",
    "ETH": {
        "balance": 444,
        "totalIn": 444,
        "totalOut": 4
    },
    "count_txs": 2,
    "tokens": [
        {
            "fst_token_info": {
                "address": "0x44444",
                "name": "fdf",
                "decimals": 0,
                "symbol": "dsfdsf",
                "total_supply": "3228562189",
                "owner": "0x44444",
                "last_updated": 1519022607901,
                "issuances_count": 0,
                "holders_count": 137528,
                "price": False
            },
            "balance": 5000,
            "totalIn": 0,
            "total_out": 0
        },
        {
            "sec_token_info": {
                "address": "0x44444",
                "name": "ggg",
                "decimals": "2",
                "symbol": "fff",
                "total_supply": "250000000000",
                "owner": "0x44444",
                "last_updated": 1520452201,
                "issuances_count": 0,
                "holders_count": 20707,
                "price": False
            },
            "balance": 500,
            "totalIn": 0,
            "total_out": 0
        }
    ]
}

print('1. Вывести списки ключей и значений словаря:')
for k, v in data.items():
    print( k, ':', v)

print('\n2. В “ETH” добавить ключ “total_diff” со значением 100.')
data['ETH']['total_diff'] = 100
print('ETH :', data['ETH'])

print('\n3. Внутри “fst_token_info” значение ключа “name” поменять с “fdf” на “doge”.')
data['tokens'][0]['fst_token_info']['name'] = 'doge'
fst_name = data['tokens'][0]['fst_token_info']['name']
print(f'fst_token_info-name: {fst_name}')

print('\n4. Удалить “total_out” из tokens и присвоить его значение в “total_out” внутри “ETH”')
total_out = 0
for i_value in data['tokens']:
    total_out += i_value.pop('total_out')
data['ETH']['total_out'] = total_out
print('ETH :', data['ETH'])

for k in data['tokens']:
    print(k)

print('\n5. Внутри "sec_token_info" изменить название ключа “price” на “total_price”.')
print('sec_token_info :', data['tokens'][1]['sec_token_info'])
old_price = data['tokens'][1]['sec_token_info'].pop('price')
data['tokens'][1]['sec_token_info']['total_price'] = old_price
print('sec_token_info :', data['tokens'][1]['sec_token_info'])



