def histogram(string):
    sym_dict = dict()
    for i in string:
        if i in sym_dict:
            sym_dict[i] += 1
        else:
            sym_dict[i] = 1
    return sym_dict

text = input('Введите текст: ')
hist = histogram(text)

print('Оригинальный словарь частот:')
for i in sorted(hist.keys()):
    print(i, ":", hist[i])

print('Инвертированный словарь частот:')
new_hist = dict()
for k, v in hist.items():
    new_hist.setdefault(v, []).append(k)

for i in new_hist.keys():
    print(i, ":",new_hist[i])

