players_dict = {
    1: {'name': 'Vanya', 'team': 'A', 'status': 'Rest'},
    2: {'name': 'Lena', 'team': 'B', 'status': 'Training'},
    3: {'name': 'Maxim', 'team': 'C', 'status': 'Travel'},
    4: {'name': 'Egor', 'team': 'C', 'status': 'Rest'},
    5: {'name': 'Andrei', 'team': 'A', 'status': 'Training'},
    6: {'name': 'Sasha', 'team': 'A', 'status': 'Rest'},
    7: {'name': 'Alina', 'team': 'B', 'status': 'Rest'},
    8: {'name': 'Masha', 'team': 'C', 'status': 'Travel'}
}

for i in players_dict.values():
    if i['team'] == 'A' and i['status'] == 'Rest':
        print('Все члены команды из команды А, которые отдыхают.', i['name'])
    if i['team'] == 'B' and i['status'] == 'Training':
        print('Все члены команды из группы B, которые тренируются.', i['name'])
    if i['team'] == 'C' and i['status'] == 'Travel':
        print('Все члены команды из команды C, которые путешествуют.', i['name'])

# team_mem = [i['name'] for i in players_dict.values() if i['team'] == 'A' and i['status'] == 'Rest']
# print(team_mem)
# team_mem = [i['name'] for i in players_dict.values() if i['team'] == 'B' and i['status'] == 'Training']
# print(team_mem)
# team_mem = [i['name'] for i in players_dict.values() if i['team'] == 'C' and i['status'] == 'Travel']
# print(team_mem)