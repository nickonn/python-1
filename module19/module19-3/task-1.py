family_member = {
    "name": "Jane",
    "surname": "Doe",
    "hobbies": ["running", "sky diving", "singing"],
    "age": 35,
    "children": [
        {
            "name": "Alice",
            "age": 6
        },
        {
            "name": "Bob",
            "age": 8
        }
    ]
}

print(family_member.get('children', {'none'}))

for i_value in family_member['children']:
    name_list = i_value.values()
    if 'Bob' in name_list:
        surname_bob = family_member.get('surname', {'Nosurname'})

        print('Нашли ', i_value.get('name', {'noname'}), surname_bob)
