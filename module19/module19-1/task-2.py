info_str = input('Введите информацию о студенте через пробел\n '
                 '(имя, фамилия, город, место учёбы, оценки): '
                 )
info_list = info_str.split()

info_dict = dict()

info_dict['Имя'] = info_list[0]
info_dict['Фамилия'] = info_list[1]
info_dict['Город'] = info_list[2]
info_dict['Место учёбы'] = info_list[3]
# info_dict['Оценки'] = info_list[4:]
info_dict['Оценки'] = []
for i in info_list[4:]:
    info_dict['Оценки'].append(int(i))

print('Результат: ')
for i in info_dict:
    print( i, '-', info_dict[i])