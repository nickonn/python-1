from collections import Counter

# def can_be_poly(s):
#     return len(s) % 2 == sum(x % 2 for x in Counter(s).values())


def can_be_poly(s):
    return len(list(filter(lambda x: x % 2, Counter(s).values()))) <= 2

print(can_be_poly('abcba'))
print(can_be_poly('abbbc'))

