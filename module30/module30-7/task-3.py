
print(*[n for n in range(1000) if n >= 2 and (n == 2 or n % 2 and all(n % i for i in range(3, int(n ** 0.5) + 1, 2)))])


def func(n):
    a = [i if i != 1 else 0 for i in range(n + 1)]
    for i in range(2, n + 1):
        if a[i] != 0:
            for j in range(i + i, n + 1, i):
                a[j] = 0
    return [x for x in a if x]


print(*func(1000))