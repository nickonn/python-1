class MyDict(dict):
    def get(self, key, default=0):
        return super().get(key, default)

        # if key in self:
        #     return self[key]
        # else:
        #     return default


new_dict = MyDict()
new_dict['Ikari'] = 1
new_dict['Asuka'] = 2
new_dict['Rei'] = 3
new_dict['as'] = 10

print(new_dict)
print(new_dict.get('ass'))

