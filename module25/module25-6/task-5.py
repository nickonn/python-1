import math


class Car:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self, dist, angle):
        self.x = self.x + dist * math.cos(angle)
        self.y = self.y + dist * math.sin(angle)
        print('Едем к координатам', (round(self.x, 2), round(self.y, 2)), 'дальность пути: ', dist)

    def __str__(self):
        return f'Координаты машины ({round(self.x, 2)} {round(self.y, 2)})'


class Bus(Car):
    PAY_COEFF = 0.01
    MAX_PASSENGERS = 60

    def __init__(self, x, y):
        super().__init__(x, y)
        self.passengers = 0
        self.money = 0

    def move(self, distance, angle):
        if 0 < angle < 180:
            moneys = Bus.PAY_COEFF * self.passengers * distance
            self.money += Bus.PAY_COEFF * self.passengers * distance
            super().move(distance, angle)
            nord = 'Север'
            print(f'Автобус проехал {distance} км на {nord} и водитель заработал {moneys}')
        elif 180 < angle < 360:
            moneys = Bus.PAY_COEFF * self.passengers * distance
            self.money += Bus.PAY_COEFF * self.passengers * distance
            super().move(distance, angle)
            south = 'Юг'
            print(f'Автобус проехал {distance} км на {south} и водитель заработал {moneys}')
        else:
            print(f'Улетел в космос на {distance} км ')

    def enter(self, passengers):
        if self.passengers + passengers > Bus.MAX_PASSENGERS:
            print(f'Максимальная вместимость автобуса 60 (человек) из {passengers} желающих уехать')
            print(f'Сели в автобус только {Bus.MAX_PASSENGERS - self.passengers}')
            print(f'Не влезло в автобус {self.passengers + passengers - Bus.MAX_PASSENGERS}')
            self.passengers = Bus.MAX_PASSENGERS
        else:
            self.passengers += passengers
            print(f'В автобус сели {passengers} пассажиров')
        return self.passengers

    def exit(self, passengers):
        if self.passengers - passengers < 0:
            print('Все вышли из автобуса')
            self.passengers = 0
        else:
            self.passengers -= passengers
            print(f'Из автобуса вышли {passengers} осталось {self.passengers} пассажиров')
        return self.passengers

    def __str__(self):
        lines = [
            f'Координаты автобуса ({round(self.x, 2)} {round(self.y, 2)})',
            f'В автобусе {self.passengers} пассажиров',
            f'Водитель заработал {round(self.money, 2)} денег всего',
        ]
        return '\n'.join(lines)


bus = Bus(x=1, y=2)
bus.enter(50)
bus.move(40, 60)
bus.exit(20)
bus.move(20, 190)
bus.exit(20)
bus.enter(70)
bus.move(30, 400)
print(bus)
bus.exit(70)
print(bus)

# import math
# import random
#
# class Car:
#     """
#     Базовый класс Car(автомобиль) описывающий движение оного.
#
#     Args:
#     x (int): передаётся координата 'х'
#     y (int): передаётся координата 'y'
#
#     Attributes:
#     x (int): координата 'x' точки нахождения Car
#     y (int): координата 'y' точки нахождения Car
#
#     """
#     def __init__(self, x, y):
#         self.x = x
#         self.y = y
#
#     def move(self, dist, angle):
#         """
#         Метод для вычисления точки конечного следования Car(автомобиля)
#
#         :param dist: передаётся дистанция на которую движется Car
#         :rtype: int
#
#         :param angle: передаётся угол направления движения Car
#         :rtype: int
#
#         """
#         self.x = self.x + int(dist * math.cos(angle))
#         self.y = self.y + int(dist * math.sin(angle))
#         print('Едем прямо', self.x, self.y, 'дальность пути: ', dist)
#
#     def turn(self, dist, angle):
#         """
#         Метод для вычисления точки следования Car(автомобиля) при смене движения
#         (
#         изменение dist - расстоянии и angle - угла направления движения
#         )
#
#         :param dist: передаётся дистанция на которую движется Car
#         :rtype: int
#
#         :param angle: передаётся угол направления движения Car
#         :rtype: int
#
#         """
#         self.x = self.x + int(dist * math.cos(angle))
#         self.y = self.y + int(dist * math.sin(angle))
#         print('Поворот не туда', self.x, self.y, 'дальность пути: ', dist)
#
#
# class Bus(Car):
#     """
#     Класс Bus(автобус). Родитель: Car(автомобиль)
#
#     __peoples: количество пассажиров
#     __many: количество заработанных денег
#     __places: количество мест в Bus(автобус)
#
#     Args:
#     x (int): передаётся координата 'х'
#     y (int): передаётся координата 'y'
#     cost (int): передаётся величина тарифа расчёта стоимости билета
#
#     Attributes:
#     cost (int): тариф для расчёта стоимости поездки
#
#     """
#     __peoples = 0
#     __many = 0
#     __places = 24
#
#     def __init__(self, x, y, cost):
#         super().__init__(x, y)
#         self.cost = cost
#
#     def more_passengers(self, entered, dist):
#         """
#         Метод запускающий __peoples(пассажир) и собирающий many(деньги) с них.
#
#         :param entered: количество вошедших пассажиров
#         :rtype: int
#
#         :param dist: дистанция движения калькуляции вместе с cost
#         :rtype: int
#
#         """
#         if Bus.__peoples >= Bus.__places:
#             print('Мест нет')
#         else:
#             Bus.__peoples += entered
#             Bus.__many += (self.cost * dist) * entered
#             print(f'Зашло {entered}, оплата {Bus.__many}')
#
#     def fewer_passengers(self, people_came_out):
#         """
#         Метод для выхода пассажиров.
#
#         :param people_came_out: количество вышедших пассажиров.
#         :rtype: int
#
#         """
#         if Bus.__peoples - people_came_out < 0:
#             print('Не может выйти больше чем есть пассажиров.')
#         else:
#             Bus.__peoples -= people_came_out
#             print(f'Вышло {people_came_out}')
#
#     def get_many(self):
#         """
#         Геттер для показа суммы many(заработанных средств) в конце поездки.
#         :return:__many
#
#         """
#         return self.__many
#
# direction = 30
# distance = 100
#
# car = Car(1, 2)
# bus = Bus(2, 4, 3)
#
# route = 0
# while route != 5:
#     route += 1
#     if random.randint(1, 2) == 2:
#         bus.move(distance, direction)
#         bus.more_passengers(random.randint(1, 5), distance)
#         bus.fewer_passengers(random.randint(1, 5))
#     else:
#         bus.turn(random.randint(1, distance), random.randint(1, direction))
#         bus.more_passengers(random.randint(1, 5), distance)
#         bus.fewer_passengers(random.randint(1, 5))
#
# print(f'Заработано {bus.get_many()}')