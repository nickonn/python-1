class CanFly:
    def __init__(self):
        self.height = 0
        self.speed = 0

    def up(self):
        pass

    def fly(self):
        pass

    def down(self):
        self.height = 0
        self.speed = 0

    def info(self):
        print(f'Высота:{self.height} Скорость:{self.speed}')


class ButterFly(CanFly):
    def __init__(self):
        super().__init__()

    def up(self):
        self.height = 1
        return self.height

    def fly(self):
        self.speed = 0.5
        return self.speed


class Rocket(CanFly):
    def __init__(self):
        super().__init__()

    def up(self):
        self.height = 500
        self.speed = 1000
        return self.height, self.speed

    def down(self):
        self.height = 0
        if self.height < 0:
            return print('Взрыв')

    def boom(self):
        print('Взрыв буум')


can = CanFly()
buterfly = ButterFly()
rocket = Rocket()
can.info()
print(buterfly.up())
print(rocket.up())
rocket.info()