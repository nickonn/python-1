class Unit:
    def __init__(self, hitpoint, uron):
        self.__hitpoint = hitpoint
        self.uron = uron

    def get_hitpoint(self):
        return self.__hitpoint

    def set_hitpoint(self, hitpoint):
        self.__hitpoint = hitpoint

    def damage(self):
        self.__hitpoint -= 0

    def __str__(self):
        return f'Колличество ХП {self.__hitpoint} Урон {self.uron}'


class Soldier(Unit):
    def __init__(self, hitpoint, uron):
        super().__init__(hitpoint, uron)

    def damage(self):
        self.set_hitpoint(self.get_hitpoint() - self.uron)

    def __str__(self):
        info = super().__str__()
        info += ' Солдат'
        return info


class Sivilian(Unit):
    def __init__(self, hitpoint, uron):
        super().__init__(hitpoint, uron)

    def damage(self):
        self.set_hitpoint(self.get_hitpoint() - self.uron * 2)

    def __str__(self):
        info = super().__str__()
        info += ' Гражданин'
        return info


unit = Unit(hitpoint=100, uron=30)
print(unit)
sold = Soldier(hitpoint=100, uron=25)
sold.damage()
print(sold)
siv = Sivilian(hitpoint=100, uron=20)
siv.damage()
print(siv)
