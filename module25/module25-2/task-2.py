
class People:
    __count = 0

    def __init__(self, name, age):
        self.set_name(name)
        self.set_age(age)
        People.__count += 1

    def get_name(self):
        return self.__name

    def get_age(self):
        return self.__age

    def set_name(self, name):
        if isinstance(name, str):
            self.__name = name
        else:
            print("Не строка")

    def set_age(self, age):
        if isinstance(age, int):
            if age in range(1, 100):
                self.__age = age
            else:
                raise Exception('Недопустимый возраст')
        else:
            print('Не число')

    def __str__(self):
        return 'Имя: {} и возраст: {} объект № = {}'.format(self.__name, self.__age, self.__count)


people = People('Маша', 20)
people.set_age(63)
print(people)
People._People__age = 50
print(People._People__age)


people1 = People('Вася', 40)
people1.set_name(12123)
print(people1)