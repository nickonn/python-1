
class Point:
    __count = 0

    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        Point.__count += 1

    def get_count(self):  #геттер
        return self.__count

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def set_x(self, x):
        if isinstance(x, int) or isinstance(x, float):
            self.__x = x
        else:
            print('Не целое число')

    def set_y(self, y):
        if isinstance(y, int) or isinstance(y, float):
            self.__y = y
        else:
            print('Не целое число')

    def __str__(self):
        return 'Координаты точки Х = {} и Y = {} точка = {}'.format(self.__x, self.__y, self.__count)



test = Point(1, 1)
test.set_x(23.3)
test.set_y(200)
print(test)
test1 = Point(1, 1)
test1.set_x(50)
test1.set_y(100)
print(test1)
print(test.get_x())
print(test.get_y())
