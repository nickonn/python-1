class Robot:
    def __init__(self, model):
        self.model = model

    def __str__(self):
        return f'Модель робота {self.model}'


class RobotDust(Robot):
    def __init__(self, model):
        super().__init__(model)
        self.box = 0

    def operate(self):
        print(f'{self.model} Начинаю пылесосить')
        self.box += 1
        print(f'Состояния мешка для пыли {self.box}')


class WarRobot(Robot):
    def __init__(self, model, gun):
        super().__init__(model)
        self.gun = gun

    def operate(self):
        print(f'{self.model} Защищаю военый объект с помощью {self.gun}')


class SubMarine(Robot):
    def __init__(self, model, gun):
        super().__init__(model)
        self.gun = gun
        super().__init__(model)
        self.depth = 0

    def operate(self):
        print(f'{self.model} Защищаю военый объект под водой с помощью {self.gun}')
        self.depth -= 1
        print(f'Текущая шлубина {self.depth}')


dust = RobotDust('vasia')
dust.operate()
warrobot = WarRobot('terminator', 'gauss')
warrobot.operate()
sub = SubMarine('poseydon', 'torpeda')
sub.operate()