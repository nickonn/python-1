class Ship:
    def __init__(self, model):
        self.model = model

    def sail(self):
        print('Корабль кудато плывет')

    def __str__(self):
        return f'Модель корабля {self.model}'


class WarShip(Ship):
    def __init__(self, model, gun):
        super().__init__(model)
        self.gun = gun

    def attack(self):
        print(f'Корабль {self.model} атакует с помощью оружия {self.gun}')


class CargoShip(Ship):
    def __init__(self, model):
        super().__init__(model)
        self.tonnage_load = 0

    def load(self):
        print('Загружаем корабль')
        self.tonnage_load += 1
        print(f'Текущая загруженность: {self.tonnage_load}')

    def unload(self):
        print('Разгружаем кораль')
        if self.tonnage_load > 0:
            self.tonnage_load -= 1
        else:
            print('Корабль разгружен')
        print(f'Текущая загруженность: {self.tonnage_load}')

warship = WarShip('ddd', 'pila')
warship.attack()

cargo = CargoShip('budka')
cargo.load()
cargo.unload()