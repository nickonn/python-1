import random


class Toyota:
    car_color = 'red'
    car_prise = 1000000
    car_speed = 200
    current_speed = 0


test_1 = Toyota()
test_1.car_speed = random.randint(0, 200)

test_2 = Toyota()
test_2.car_speed = random.randint(0, 200)

test_3 = Toyota()
test_3.car_speed = random.randint(0, 200)

print(test_1.car_speed)
print(test_2.car_speed)
print(test_3.car_speed)