
class Monitor:
    monitor_name = 'Samsung'
    monitor_matrix = 'VA'
    monitor_res = 'WQHD'
    monitor_freq = 60

class HeadPhones:
    headphones_name = 'Sony'
    headphones_sensitivity = 108
    headphones_micro = False

monitor = Monitor()

monitor_1 = Monitor()
monitor_1.monitor_freq = 144

monitor_2 = Monitor()
monitor_2.monitor_freq = 70

monitor_3 = Monitor()
monitor_3.monitor_freq = 60

headphones = HeadPhones()

headphones_1 = HeadPhones()
headphones_1.headphones_micro = True

headphones_2 = HeadPhones()
headphones_2.headphones_micro = True

print(monitor.monitor_freq)
print(monitor_1.monitor_freq)
print(monitor_2.monitor_freq)
print(monitor_3.monitor_freq)

print(headphones.headphones_micro)
print(headphones_1.headphones_micro)
print(headphones_2.headphones_micro)