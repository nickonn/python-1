class Toyota:

    def __init__(self, car_color, car_prise, car_speed, current_speed):
        self.car_color = car_color
        self.car_prise = car_prise
        self.car_speed = car_speed
        self.current_speed = current_speed


    def info(self):
        print(f' car_color = {self.car_color},\n car_prise = {self.car_prise},\n '
              f'car_speed = {self.car_speed},\n current_speed = {self.current_speed}')

    def speed(self, num):
        self.current_speed = num
        print(f'Now speed change: {self.current_speed}')


test = Toyota('red', 1000000, 200, 0)

test.info()
