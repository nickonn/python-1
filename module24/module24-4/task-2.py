
class Point:

    count = 0

    def __init__(self, y=0, x=0):
        self.y = y
        self.x = x
        Point.count += 1

    def info(self):
        print('Координаты точки Х = {} и Y = {} точка = {}'.format(self.x, self.y, self.count))


test = Point(13, 200)
test.info()
test = Point(132, 3)
test.info()
test = Point(25, 25)
test.info()