
class Toyota:
    car_color = 'red'
    car_prise = 1000000
    car_speed = 200
    current_speed = 0

    def info(self):
        print(f' car_color = {self.car_color},\n car_prise = {self.car_prise},\n '
              f'car_speed = {self.car_speed},\n current_speed = {self.current_speed}')

    def speed(self, num):
        self.current_speed += num
        print(f'Now speed change: {self.current_speed}')


test = Toyota()

test.info()
test.speed(100)
test.info()
