class Family:
    family_name = 'Common family'
    family_funds = 100000
    having_a_house = False

    def info(self):
        print(' family_name = {}\n family_funds = {}\n having_a_house = {}'.format(
            self.family_name, self.family_funds, self.having_a_house))

    def money_plus(self, money):
        self.family_funds += money
        print('Plus {} Now family_funds change: {}'.format(money, self.family_funds))

    def buy_house(self, house_prise, discount=0):
        house_prise -= house_prise * discount / 100
        if self.family_funds >= house_prise:
            self.family_funds -= house_prise
            self.having_a_house = True
            print('House buy, current money: {}\n'.format(self.family_funds))
        else:
            print('Not enough money!\n')


test = Family()

test.info()

print('\nTry to buy a house')
test.buy_house(1000000)

if not test.having_a_house:
    test.money_plus(800000)
    print('try to buy a house again')
    test.buy_house(1000000, 10)

test.info()
