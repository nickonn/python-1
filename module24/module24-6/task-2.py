
class Student:
    def __init__(self, full_name, number_g, score):
        self.full_name = full_name
        self.number_g = number_g
        self.score = score

    def info(self):
        print(f'{self.full_name} , {self.number_g}, {self.score} ')

    def average_score(self):
        return sum(self.score) / len(self.score)

    def __str__(self):
        result = f'{self.full_name} {self.number_g} {self.average_score()}'
        return result


students = []

for i in range(2):
    print(f'Студент {i + 1}: ')
    full_name = input('имя: ')
    group_n = int(input('номер группы: '))
    academic_p = list(map(int, input("оценки: ").split()))
    students.append(Student(full_name, group_n, academic_p))

sort = sorted(students, key=lambda student: student.average_score())
print(*sort, sep='\n')
