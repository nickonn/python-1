
class Cell:

    def __init__(self):
        self.win_coord = ((0, 1, 2), (3, 4, 5),
                          (6, 7, 8), (0, 3, 6),
                          (1, 4, 7), (2, 5, 8),
                          (0, 4, 8), (2, 4, 6))

    def check_win(self):
        for each in self.win_coord:
            if Board.board[each[0]] == Board.board[each[1]] == Board.board[each[2]]:
                return Board.board[each[0]]
        return False


class Board:
    board = list(range(1, 10))

    def draw_board(self):
        print('-' * 13)
        for i in range(3):
            print("|", self.board[0 + i * 3], "|",
                  self.board[1 + i * 3], "|",
                  self.board[2 + i * 3], "|")
            print("-------------")


class Player:

    def __init__(self, name, symbol):
        self.symbol = symbol
        self.name = name

    def take_input(self):
        valid = False
        while not valid:
            player_answer = input(self.name + ': ' + "Куда поставим " + self.symbol + "? ")
            try:
                player_answer = int(player_answer)
            except:
                print("Некорректный ввод. Вы уверены, что ввели число?")
                continue
            if player_answer >= 1 and player_answer <= 9:
                if (str(Board.board[player_answer - 1]) not in "XO"):
                    Board.board[player_answer - 1] = self.symbol
                    valid = True
                else:
                    print("Эта клеточка уже занята")
            else:
                print("Некорректный ввод. Введите число от 1 до 9 чтобы походить.")

def game():
    board = Board()
    cell = Cell()
    player1 = Player('Vasia', "X")
    player2 = Player('Petia', "O")
    counter = 0
    win = False
    while not win:
        board.draw_board()
        if counter % 2 == 0:
            player1.take_input()
        else:
            player2.take_input()
        counter += 1
        if counter > 4:
            tmp = cell.check_win()
            if tmp:
                if tmp == 'X':
                    print(player1.name, "выиграл!",)
                    win = True
                    break
                else:
                    print(player2.name, "выиграл!",)
                    win = True
                    break
        if counter == 9:
            print( "Ничья!")
            break
    board.draw_board()


game()
