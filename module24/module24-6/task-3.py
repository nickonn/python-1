
class Circle:
    pi = 3.14159

    def __init__(self, x=0, y=0, r=1):
        self.x = x
        self.y = y
        self.r = r

    def info(self):
        print(f'X={self.x}, Y={self.y}, радиус={self.r}')

    def get_area(self):
        return self.r * self.r * self.pi

    def get_perimeter(self):
        return 2 * self.r * self.pi

    def scale(self, k):
        self.r *= k

    def is_intersect(self, other):
        return (self.x - other.x) ** 2 + (self.y - other.y) ** 2 <= (self.r + other.r) ** 2


circle = Circle()
r = int(input('Введите радиус 1-го круга: '))
circle.scale(r)
circle.info()
print('Площадь круга:', circle.get_area(), 'Периметр круга:', circle.get_perimeter())

print('------- Втогой круг ------')
circle_1 = Circle(2, 2, 1)
circle_1.info()
print('Площадь круга:', circle_1.get_area(), 'Периметр круга:', circle_1.get_perimeter())
if circle_1.is_intersect(circle):
    print('Пересекается')
else:
    print('Не пересекается')

