
list_1 = [2, 5, 7, 10]
list_2 = [3, 8, 4, 9]

to_find = 56
can_continue = True

def gen_calc():
    for x in list_1:
        for y in list_2:
            result = x * y
            yield x, y, result

gen = gen_calc()
# print(*gen)

for i in gen:
    print(i[0], '*', i[1], '=', i[2])
    if i[2] == to_find:
        print(i[0], '*', i[1], '=', i[2], 'found')
        break



