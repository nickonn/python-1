from pathlib import Path

directory = Path(r'D:\PycharmProjects\python-1\module26')
line_count = 0

for f in directory.rglob('*.py'):
    if not f.is_file() or not f.exists():
        continue

    local_count = 0
    for line in f.read_text().splitlines():
        line = line.strip()
        if not line or line.startswith(('#', '"', "'")):
            continue
        local_count += 1

    print(f'{f} - {local_count} ст')
    line_count += local_count

print("=====================================")
print(f"Всего строк - {line_count}")

# import os
#
# strings = 0
#
# files = os.listdir('D:\PycharmProjects\python-1\module26\module26-7')
# print(files)
# for file in files:
#     if os.path.isfile(file):
#         if file.endswith('.py'):
#             with open(file) as f:
#                 ss = f.read().split('\n')
#                 for s in ss:
#                     if s.strip() and not s.startswith('#') and not s.startswith('"') and not s.startswith("'"):
#                         strings += 1
#
# print('Всего строк:', strings )