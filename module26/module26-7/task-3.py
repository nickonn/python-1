import os


def gen_files_path(path: str):
    print('---Текущая директория', path)

    for i_elem in os.listdir(path):
        current_path = os.path.join(path, i_elem)

        if os.path.isdir(current_path):
            yield from gen_files_path(current_path)

        elif os.path.isfile(os.path.join(path, i_elem)):
            yield os.path.join(path, i_elem)


user_folder = 'PycharmProjects\python-1\module26'
abs_path = os.path.abspath(os.path.join(os.path.sep, user_folder))
print(os.listdir(abs_path))
result = gen_files_path(path=abs_path)

for i_path in result:
    print(i_path)

# def recursive_walk(folder):
#     for folderName, subfolders, filenames in os.walk(folder):
#         if subfolders:
#             for subfolder in subfolders:
#                 recursive_walk(subfolder)
#         print('\nFolder: ' + folderName + '\n')
#         for filename in filenames:
#             print(filename)
#
# recursive_walk(abs_path)


# user_folder1 = 'PycharmProjects\python-1'
# abs_path1 = os.path.abspath(os.path.join(os.path.sep, user_folder1))
# print(abs_path1)
# for dirname, dirnames, filenames in os.walk(abs_path1):
#
#     for subdirname in dirnames:
#         print(os.path.join(dirname, subdirname))
#
#     for filename in filenames:
#         print(os.path.join(dirname, filename))
#
#     if '.git' in dirnames:
#         dirnames.remove('.git')
