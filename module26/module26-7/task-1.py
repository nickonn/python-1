from collections.abc import Iterable


class ImIterator:
    def __init__(self, limit: int) -> None:
        self.counter = 0
        self.limit = limit

    def __iter__(self):
        return self

    def __next__(self) -> int:
        self.counter += 1
        if self.counter == self.limit:
            raise StopIteration
        else:
            return self.counter ** 2


def gen_func(limit: int) -> Iterable[int]:
    for i in range(limit):
        yield i ** 2

print('Функция генератор')
gen = gen_func(limit=10)
for i in gen:
    print(i, end=' ')
# print(*gen)

print('\nКласс итератор')
cl = ImIterator(limit=10)
print(*cl)

print('Генераторное выражение')
gen_cicle = (x ** 2 for x in range(1, 10))
print(*gen_cicle)