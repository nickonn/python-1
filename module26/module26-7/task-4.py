
class Hofstadter_sequence:
    def __init__(self, seq=[1, 1]):
        self.__seq = self.validate(seq)
        self.__cnt = 2

    def validate(self, arg):
        if isinstance(arg, list) and len(arg) == 2:
            return arg
        else:
            print('Недопустимый аргумент: аргумент должен быть списком с 2 значениями')

    def __next__(self):
        self.__cnt += 1
        try:
            new_q = self.__seq[self.__cnt - self.__seq[self.__cnt - 2] - 1] + self.__seq[
                self.__cnt - self.__seq[self.__cnt - 3] - 1]
            self.__seq.append(new_q)
            return self.__seq
        except IndexError:
            raise StopIteration()

    def __iter__(self):
        return self

    def current_state(self):
        return self.__seq


aa = Hofstadter_sequence()

for i in aa:
    print(i)


