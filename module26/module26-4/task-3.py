import math


class Primes():
    def __init__(self, bound):
        self.nums = list(range(2, bound))

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.nums) == 0:
            raise StopIteration

        next_prime = self.nums[0]
        self.nums = [x for x in self.nums if x % next_prime != 0]
        return next_prime


# prime_nums = Primes(20)
# for i_elem in prime_nums:
#     print(i_elem, end=' ')


# class PrimesNext():
#     def __init__(self, bound):
#         self.nums = bound
#         self.count = 2
#
#     def __iter__(self):
#         return self
#
#     def __next__(self):
#         if self.count >= self.nums:
#             raise StopIteration
#
#         self.count += 1
#         for x in range(2, int(math.sqrt(self.nums) + 1)):
#             if self.count % x == 0:
#                 break
#             else:
#                 return self.count
#         return ''
#
# prime_nums = PrimesNext(20)
# for i_elem in prime_nums:
#     print(i_elem, end=' ')

prime_nums = Primes(15)
for i_elem in prime_nums:
    print(i_elem, end=' ')


# class PrimeIter:
#     def __init__(self, max):
#         self.max = max
#
#     def __iter__(self):
#         self.n = 1
#         return self
#
#     def __next__(self):
#         if self.n < self.max:
#             self.n += 1
#             i = 2
#             while i < self.n:
#                 if self.n % i == 0:
#                     self.n = self.n+1
#                     if self.n > self.max:
#                         raise StopIteration
#                     i = 1
#                 i += 1
#             else:
#                 return self.n
#         else:
#             raise StopIteration
#
#
# p = PrimeIter(15)
# for i in p:
#     print(i, end=' ')
