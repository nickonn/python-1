import random


class RandomGenerator:
    def __init__(self, elem):
        self.elem = elem
        self.counter = 0
        self.result = 0

    def __iter__(self):
        self.counter = 0
        self.result = 0
        return self

    def __next__(self):
        self.counter += 1
        if self.counter > self.elem:
            raise StopIteration
        if self.result == 0:
            self.result = random.random()
            return round(self.result, 2)
        else:
            self.result += random.random()
            return round(self.result, 2)


num = int(input('Введите число: '))
my_iter = RandomGenerator(elem=num)
print('Элементы итератора:')
for i_elem in my_iter:
    print(i_elem)
