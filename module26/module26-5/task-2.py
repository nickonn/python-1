def reader(file_name):
    with open(file_name, "r", encoding='utf-8') as file:
        for row in file:
            yield row


name_file ='numbers.txt'
list_nums = []

# list_nums = reader(name_file).__next__().split()
for i in reader(name_file):
    list_nums += i.split()
print(list_nums)

s = 0
for i in list_nums:
    s += int(i)
print('Сумма:', s)


