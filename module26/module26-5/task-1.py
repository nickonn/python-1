import sys


class ImIterator:
    def __init__(self):
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.counter += 1
        yield self.counter - 1


my_iter = ImIterator()
print('size', sys.getsizeof(my_iter))
for i_elem in my_iter:
    print(*i_elem)


# def gen():
#     num = 0
#     while True:
#         yield num
#         num += 1
#
#
# aa = gen()
# print(*aa)

