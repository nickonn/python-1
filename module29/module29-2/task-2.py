import os
from contextlib import contextmanager


# @contextmanager
# def in_dir(my_path):
#     try:
#         for my_dirs in os.listdir(my_path):
#             if os.path.isdir(os.path.join(my_path, my_dirs)):
#                 print(f'{my_path}{my_dirs}')
#     except FileNotFoundError as ex:
#         print(f'Такого пути нет ошибка {ex}')
#     yield print(os.getcwd())

@contextmanager
def in_dir(path):
    cur_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(cur_path)


with in_dir('C:\\'):
    print(os.listdir())

