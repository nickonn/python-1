import time
from contextlib import contextmanager


# class Timer():
#
#     def __init__(self):
#         print('Время работы кода')
#         self.start = None
#
#     def __enter__(self):
#         self.start = time.time()
#         return self
#
#     def __exit__(self, ext_type, ext_val, ext_tb):
#         print(time.time() - self.start)
#         return True

@contextmanager
def timer():
    start =time.time()
    try:
        yield
    except Exception as ex:
        print(ex)
    finally:
        print(time.time() - start)


with timer() as t1:
    print('Первая часть')
    val_1 = 100 * 100 ** 1000000
    val_1 += 'dd'

with timer() as t2:
    print('Вторая часть')
    val_2 = 200 * 200 ** 1000000

with timer() as t3:
    print('Третья часть')
    val_3 = 300 * 300 ** 1000000