
def check_permission(user):
    def decorator(func):

        def wrapped_func():
            if user in user_permissions:
                func()
            else:
                print(f'У пользователя {user} недостаточно прав, '
                                      f'чтобы выполнить функцию {func.__name__}')

        return wrapped_func

    return decorator


user_permissions = ['admin']


@check_permission('admin')
def delete_site():
    print('Удаляем сайт')


@check_permission('user_1')
def add_comment():
    print('Добавляем комментарий')


delete_site()
add_comment()