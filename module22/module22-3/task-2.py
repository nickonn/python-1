import os


def find_file(cur_path, file_name):
    print("Переходим:", cur_path)
    see = ''
    for i in os.listdir(cur_path):
        path = os.path.join(cur_path, i)
        print("Смотрим:", path)
        if file_name == i:
            see = path

    else:
        result = None
    result = see
    return result

file_path = find_file(os.path.abspath(os.path.join('..', 'module22-3')), 'readme.txt')

if file_path:
    print("Абсолютный путь к файлу:", file_path)
    file = open(file_path, 'r', encoding='utf-8')
    print("------------------------")
    print(file.read())
    print("------------------------")
    file.close()
else:
    print("Файл не найден")
