import os

file_path1 = os.path.abspath(os.path.join('task', 'group_1.txt'))
# file_path2 = os.path.abspath(os.path.join('Additional_info', 'group_2.txt'))
file = open(file_path1, 'r', encoding='utf-8')
summa = 0

for i_line in file:
    info = i_line.split()
    summa += int(info[2])
file.close()

file = open(file_path1, 'r', encoding='utf-8')
diff = 0

for i_line in file:
    info = i_line.split()
    diff -= int(info[2])
file.close()

file_2 = open('Additional_info\group_2.txt', 'r', encoding='utf-8')
compose = 1

for i_line in file_2:
    info = i_line.split()
    compose *= int(info[2])
file_2.close()

print('Сумма очков 1-й группы', summa)
print('Разность очков 1-й группы', diff)
print('Произведение очков 2-й группы', compose)


