import os


def print_dir(project):
    print("\nСодержимое директории:", project)
    if os.path.exists(project):
        for i in os.listdir(project):
            path = os.path.join(project, i)
            print(" ----> ", path)
    else:
        print('Каталога не существует')

name_dir = ['module20', 'module21', 'ddddd']

for i in name_dir:
    patch_to_dir = os.path.abspath(os.path.join('..', '..', i))
    print_dir(patch_to_dir)






