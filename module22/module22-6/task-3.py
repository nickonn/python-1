import string

file_read = open('zen.txt', 'r', encoding='utf-8')
data = file_read.read()

chars = [i for i in data.lower() if i in string.ascii_letters]
# chars = sum(map(str.isalpha, data))
words = data.split()
lines = data.splitlines()
rare_sym = min(chars, key=lambda x: chars.count(x))

file_read.close()


print('Количество букв в файле:', len(chars))
print('Количество слов в файле:', len(words))
print('Количество строк в файле:', len(lines))
print('Наиболее редкая буква:', rare_sym)