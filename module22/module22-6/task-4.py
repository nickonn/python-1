import os

sum_size_dir = 0
size_dir = 0
kol_all_dir = 0
sum_files = 0
test_path = input('Введите адрес: ')

if os.path.exists(test_path):

    for root, dirs, files in os.walk(test_path):
        print('\nПереходим в Каталог', root)

        for i in os.listdir(root):
            if os.path.isdir(os.path.join(root, i)):
                print('Каталог', i)

        for filename in files:
            file_size = os.path.getsize(os.path.join(root, filename)) // 1024
            size_dir += file_size
            sum_files += 1
            print('-->файл', filename, '   |Размер:', file_size, 'Кб')

        if not len(files) == 0:
            print('\nКаталог', root, '\n--Размер каталога (в Кб):', size_dir,
                  '--Количество подкаталогов:', len(dirs),
                  '--Количество файлов:', len(files))

        kol_all_dir += len(dirs)
        sum_size_dir += size_dir
        size_dir = 0

    print('\nИтого по каталогу', test_path, '\n--Размер каталога (в Кб):', sum_size_dir,
          '\n--Количество подкаталогов:', kol_all_dir,
          '\n--Количество файлов:', sum_files)

else:
    print('Объект не найден')