import string


def cipher(text, key, characters=string.ascii_lowercase + string.ascii_uppercase):
    if key < 0:
        print("key cannot be negative")
        return None
    n = len(characters)
    table = str.maketrans(characters, characters[key:]+characters[:key])
    translated_text = text.translate(table)
    return translated_text

file_read = open('text.txt', 'r', encoding='utf-8')
file_write = open('cipher_text.txt', 'w', encoding='utf-8')
shift = 0
for line in file_read:
    shift += 1
    lineNew = cipher(line, shift)
    file_write.write(lineNew)
file_read.close()
file_write.close()

file_read = open('text.txt', 'r', encoding='utf-8')
file_read_clipher = open('cipher_text.txt', 'r', encoding='utf-8')
print('Содержимое файла text.txt:\n', file_read.read())
print('Содержимое файла cipher_text.txt:\n', file_read_clipher.read())
file_read.close()
file_read_clipher.close()


# def fileCipher(fileName, outputFileName, key):
#     with open(fileName, "r") as f_in:
#         with open(outputFileName, "w") as f_out:
#             for line in f_in:                 # итерация по каждой строке входного файла
#                 key += 1
#                 lineNew = cipher(line, key)    #шифровать/дешифровать строку
#                 f_out.write(lineNew)           #записать новую строку в выходной файл
#     print("The file {} has been translated successfully and saved to {}".format(fileName, outputFileName))
#
# inputFile = 'text.txt'
# outputFile = 'cipher_text.txt'
#
# fileCipher(inputFile, outputFile, key=0)