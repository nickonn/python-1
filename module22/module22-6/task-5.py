import os

def safe_file(str_path, str_file):
    if os.path.exists(str_path):

        for root, dirs, files in os.walk(str_path):
            print('\nПереходим в Каталог', str_path)
            if str_file in files:
                ask = input('Файл с таким именем уже есть, перезаписать его? Y/N: ').lower()
                if ask == 'y':
                    file_write = open(os.path.join(str_path, str_file), 'w', encoding='utf-8')
                    file_write.write(text)
                    file_write.close()
                    print('Файл успешно сохранён!')
                    break
                else:
                    print('Файл не сохранён!')
                    break
            else:
                file_write = open(os.path.join(str_path, str_file), 'w', encoding='utf-8')
                file_write.write(text)
                file_write.close()
                print('Файл успешно сохранён!')

    else:
        print('Объект не найден')

text = input('Введите строку: ')
path = input('Куда хотите сохранить документ? ').split(' ')
name_file = input('Введите имя файла: ')

root_path = os.path.abspath(os.path.sep)
path_text = root_path + (os.path.sep.join(path))

safe_file(path_text, name_file)

if os.path.exists(path_text):
    file_read = open(os.path.join(path_text, name_file), 'r', encoding='utf-8')
    read_text = file_read.read()
    print('Содержимое файла:\n', read_text)
    file_read.close()
