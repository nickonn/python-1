import string
import zipfile

def histogram(string):
    sym_dict = dict()
    for i in string:
        if i in sym_dict:
            sym_dict[i] += 1
        else:
            sym_dict[i] = 1
    return sym_dict

with zipfile.ZipFile('voyna-i-mir.zip', 'r') as zip_file:
    zip_file.extract('voyna-i-mir.txt', '.')
# archive = zipfile.ZipFile('warendpeace.zip', 'r')
# archive.extract('warendpeace.txt', '.')

file_read = open('voyna-i-mir.txt', 'r', encoding='utf-8')
data = file_read.read()

rus_letter = []
for sym in data:
    if ord('а') <= ord(sym) <= ord('я') or sym == 'ё' or sym == '':
        rus_letter += sym
    if ord('А') <= ord(sym) <= ord('Я') or sym == 'Ё':
        rus_letter += sym
eng_letter = [i for i in data if i in string.ascii_letters]
digit_letter = [i for i in data if i in string.digits]

n_letter = []
for sym in data:
    n_letter += sym.replace('\n', '').replace(' ', '')
another_letter = list(set(n_letter) - set(rus_letter) - set(eng_letter) - set(digit_letter))
ano_sym = [i for i in data if i in another_letter]

print('Количество других символов в файле:', len(ano_sym))
print('Количество цифр в файле:', len(digit_letter))
print('Количество символов eng в файле:', len(eng_letter))
print('Количество символов rus в файле:', len(rus_letter))
file_read.close()

ano_dict = histogram(ano_sym)
dig_dict = histogram(digit_letter)
eng_dict = histogram(eng_letter)
rus_dict = histogram(rus_letter)

ano_list = sorted(ano_dict.items(), key=lambda item: item[1])
dig_list = sorted(dig_dict.items(), key=lambda item: item[1])
eng_list = sorted(eng_dict.items(), key=lambda item: item[1])
rus_list = sorted(rus_dict.items(), key=lambda item: item[1])

print('Частота других символов:\n ', list(reversed(ano_list)))
print('Частота цифр:\n ', list(reversed(dig_list)))
print('Частота eng букв:\n ', list(reversed(eng_list)))
print('Частота rus букв:\n ', list(reversed(rus_list)))

res = dict(rus_list)

print("+{:-^19}+".format('+'))
print("|{: ^9}|{: ^9}|".format('буква', 'частота'))
print("+{:-^19}+".format('+'))
for k, v in reversed(res.items()):
    print("|{: ^9}|{: ^9}|".format(k, v))
print("+{:-^19}+".format('+'))








