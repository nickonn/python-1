import string


def histogram(string):
    sym_dict = dict()
    for i in string:
        if i in sym_dict:
            sym_dict[i] += 1
        else:
            sym_dict[i] = 1
    return sym_dict

file_read = open('text1.txt', 'r', encoding='utf-8')
file_write = open("analysis.txt", "w", encoding='utf-8')

data = file_read.read().lower()
print('Текст из файла:\n', data)

chars = [i for i in data if i in string.ascii_letters]
print('Количество букв в файле:', len(chars))
file_read.close()

hist = histogram(chars)
print('Словарь: ', hist)

print('Отсортированный список частот:')
new_list = sorted(hist.items(), key=lambda item: item[0])
new_list.sort(key=lambda item: item[1], reverse=True)

sout = ''
for i in new_list:
    result = (i[0] + " " + str(round(i[1] / len(chars), 3)) + "\n")
    print(result)
    file_write.write(result)

# str = open("text1.txt", "r").read().lower()
# f = {}
# n = 0
# for c in str:
#    if ord('a') <= ord(c) <= ord('z'):
#        x = f.get(c, 0)
#        f[c] = x + 1
#        n += 1
#
# print('словарь:', f)
# lout = [(k, "{:5.3f}".format(f[k]/n)) for k in f.keys()]
#
# lout.sort(key=lambda x: x[0])
# lout.sort(key=lambda x: x[1], reverse=True)
#
# sout = "\n".join([i[0] + " " + i[1] for i in lout])
# print(sout)
# open("analysis.txt", "w").write(sout)









