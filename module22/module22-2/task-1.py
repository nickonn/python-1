import os

find_name = 'task-1.py'

find_path = os.path.abspath(find_name)
print('Ищем:', find_path)

if os.path.exists(find_path):
    if os.path.isdir(find_path):
        print('Это директория:', find_path)

    if os.path.isfile(find_path):
        print('Это файл:', find_path, 'Размер файла:', os.path.getsize(find_path), 'Байт')

    if os.path.islink(find_path):
        print('Это ссылка:', find_path)
else:
    print('Пути не существует')


