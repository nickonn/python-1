import os


def find_file(cur_path, file_name):
    print("Переходим:", cur_path)

    for i in os.listdir(cur_path):
        path = os.path.join(cur_path, i)
        print("Смотрим:", path)
        if file_name == i:
            return path
        if os.path.isdir(path):
            print("Это директория")
            result = file_name(path, file_name)
            if result:
                break
    else:
        result = None
    return result

file_path = find_file(os.path.abspath(os.path.join('..', '..', 'module22', 'module22-1')), 'task-3.py')

if file_path:
    print("Абсолютный путь к файлу:", file_path)
else:
    print("Файл не найден")

