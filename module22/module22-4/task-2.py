import os

def find_file(cur_path, file_name):
    print("Переходим:", cur_path)
    see = ''
    for i in os.listdir(cur_path):
        path = os.path.join(cur_path, i)
        print("Смотрим:", path)
        if file_name == i:
            see = path

    else:
        result = None
    result = see
    return result


select_file_dir = os.path.abspath(os.path.join('python_basic'))
for i in os.listdir(select_file_dir):
    file_path = find_file(os.path.abspath(os.path.join('python_basic')), i)

    if file_path:
        print("Абсолютный путь к файлу:", file_path)
        file_read = open(file_path, 'r', encoding='utf-8')
        text = file_read.read()

        scripts_write = open('scripts.txt', 'a', encoding='utf-8')
        scripts_write.write('\n')
        scripts_write.write('*' * 40)
        scripts_write.write('\n')
        scripts_write.write(text)

        file_read.close()
        scripts_write.close()
    else:
        print("Файл не найден")
