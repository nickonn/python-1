list_prices = [1.09, 23.56, 57.84, 4.56, 6.78]

first_per = int(input('Повышение на первый год: '))
second_per = int(input('Повышение на второй год: '))

prices_first = [price * (1 + first_per /100 ) for price in list_prices]
prices_second = [price * (1 + second_per /100 ) for price in list_prices]

print('Сумма цен за каждый год:', round(sum(list_prices),2), round(sum(prices_first),2), round(sum(prices_second),2))