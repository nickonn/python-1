a = int(input('Левая граница: '))
b = int(input('Правая граница: '))

list_kub = [a ** 3 for a in range(a, b)]
list_sqare = [a ** 2 for a in range(a, b)]

print('Список кубов чисел в диапазоне от', a, 'до', b, ':', list_kub)
print('Список квадратов чисел в диапазоне от', a, 'до', b, ':', list_sqare)