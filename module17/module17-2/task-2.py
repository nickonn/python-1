list_str = list(input('Введите строку: '))
sym = input('Введите дополнительный символ: ')

list_plus_sym = list_str

list_str = [list_str[a] * 2 for a in range(len(list_str))]
list_plus_sym = [list_plus_sym[a] * 2 + sym for a in range(len(list_plus_sym))]

print('Список удвоенных символов:', list_str)
print('Склейка с дополнительным символом:', list_plus_sym)