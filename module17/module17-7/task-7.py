
num_list = [i for i in range(13)]
new_list = []

for n in range(1, len(num_list) // 3+1):
    new_list += [[i for i in range(n, len(num_list), len(num_list) // 3)]]

print('Список до:', num_list)
print('Список после:', new_list)

