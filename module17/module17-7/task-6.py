import  random

kol = int(input('Количество чисел в списке: '))

num_list = [random.randint(0, 2) for _ in range(kol)]
new_list = [i for i in num_list if i != 0] + [i for i in num_list if i == 0]
new_list = [i for i in new_list if i != 0]

print('Список до сжатия: ', num_list)
print('Список после сжатия: ',new_list)