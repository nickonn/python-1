while True:
    grats_template = input('Введите шаблон поздравления, '
                           'в шаблоне можно использовать конструкцию'
                           ' {name} и {age}:')
    if '{name}' in grats_template and '{age}' in grats_template:
        break
    print('Ошибка: отсутствует конструкция')

name_list = input('Введите: Список людей через запятую:').split(', ')
ages_str = input('Введите: Возраст людей через пробел:')

ages = [int(i) for i in ages_str.split()]

for i in range(len(name_list)):
    print(grats_template.format(name=name_list[i], age=ages[i]))

people = [', '.join([name_list[i], str(ages[i])])
              for i in range(len(name_list))
]

people_str = ', '.join(people)
print('Именинники', people_str)
