text = input('Введите строку: ')

count_lo = 0
count_up = 0

for i in text:
    if i.islower():
        count_lo += 1
    if i.isupper():
        count_up += 1

if count_lo > count_up:
    print(text.lower())
else:
    print(text.upper())


