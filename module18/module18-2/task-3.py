ip_address = input('Введите шаблон IP адреса, шаблон {num}. ')

ip_list = []

for i in range(4):
    num = input(f'Введите {i + 1} число: ')

    if int(num) < 0 or int(num) > 255:
        print('Такого IP адреса не существует')
        print('Введите число от 0 до 255')

    else:
        ip_list.append(num)

print(ip_address.format(num='.'.join(ip_list)))