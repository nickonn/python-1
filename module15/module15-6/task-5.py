films = ['Крепкий орешек', 'Назад в будущее', 'Таксист', 'Леон', 'Богемская рапсодия', 'Город грехов', 'Мементо', 'Отступники' , 'Деревня' ]
films_like = []

a = int(input('Сколько фильмов хотите добавить? '))

for i in range(a):
    name_film = input('Введите название фильма: ')
    for n in films:
        if name_film == n:
            films_like.append(n)
        if name_film not in films:
            print('Ошибка: фильма', name_film, 'у нас нет :(')
            break

print('Ваш список любимых фильмов:', films_like)