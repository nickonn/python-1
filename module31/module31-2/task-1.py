import re


text = 'How much wood would a woodchuck chuck if a woodchuck could chuck wood?'

result = re.match(r'wo', text) # Определить, начинается ли текст с шаблона wo
print('1.Определить, начинается ли текст с шаблона:', result)

result = re.search(r'wo', text)
print('2.Найти первое упоминание шаблона wo в тексте:', result)

print('3.Определить содержимое найденной по шаблону подстроки из пункта 2:', result.group())

print('4.Начальная позиция:', result.start())
print('5.Конечная позиция:', result.end())

result = re.findall('wo', text)
print('6.Получить список из каждого упоминания шаблона wo в тексте:', result)

result = re.sub(r'wo', 'ЗАМЕНА', text)
print('7.Заменить в тексте все совпадения по шаблону wo на слово «ЗАМЕНА»:', result)
