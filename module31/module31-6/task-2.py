import re


text = 'А578ВЕ777 ОР233787 К901МН666 СТ46599 СНИ2929П777 666АМР666'


result = re.findall(r"\b[АВЕКМНОРСТУХ]\d{3}\w{2}\d{2,3}\b", text)
print('Список номеров частных автомобилей:', result)

result_1 = re.findall(r"\b[АВЕКМНОРСТУХ]{2}\d{5,6}\b", text)
print('Список номеров такси:', result_1)

