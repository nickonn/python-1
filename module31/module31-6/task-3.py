import requests
import json

max_death = 0
new_dict = dict()

result = requests.get("https://www.breakingbadapi.com/api/deaths")
json_dict = json.loads(result.text)
with open("swap_3.json", "w") as file:
    json_text = json.dump(json_dict, file, indent=4)  # сериализация JSON

for elem in json_dict:
    if elem['number_of_deaths'] > max_death:
        max_death = elem['number_of_deaths']
print('Максимум трупов:', max_death)
print('-----' * 20)

for elem in json_dict:
    if elem['number_of_deaths'] == max_death:
        with open('max_death.json', 'w') as file:
            json.dump(elem, file, indent=4)

with open('max_death.json', 'r') as file:
    data = json.load(file)

new_dict['death_id'] = data['death_id']
new_dict['season'] = data['season']
new_dict['episode'] = data['episode']
new_dict['number_of_deaths'] = data['number_of_deaths']
new_dict['death'] = data['death']

for elem in new_dict:
    print(elem, ':', new_dict[elem])

