import requests
import json
# import swapi


# print(swapi.get_film(1))
# Сейчас библиотека не работает, получить начало сюжета можно напрямую

result_1 = requests.get("https://swapi.dev/api/films/")
json_dict = json.loads(result_1.text)  # десериализация JSON
with open("swap_1.json", "w") as file:
    json_text = json.dump(json_dict, file, indent=4)  # сериализация JSON
print(json_dict['results'][0]['title'])


result = requests.get("https://swapi.dev/api/films/1/")
json_dict = json.loads(result.text)
print(json_dict["opening_crawl"])
