class Date:
    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year

    def __str__(self):
        return f'день: {self.day} месяц: {self.month} год: {self.year}'

    @classmethod
    def from_string(cls, date: str) -> 'Date':
        # date_list = date.split('-')
        # day, month, year = int(date_list[0]), int(date_list[1]), int(date_list[2])
        day, month, year = map(int, date.split('-'))
        day_obj = cls(day, month, year)
        return day_obj

    @classmethod
    def is_date_valid(cls, date: str) -> bool:
        date_list = date.split('-')
        day, month, year = int(date_list[0]), int(date_list[1]), int(date_list[2])
        if 0 < day <= 31 and 0 < month <= 12 and 0 < year <= 9999:
            return True
        else:
            return False


date = Date.from_string('10-12-2077')
print(date)
print(Date.is_date_valid('10-12-2077'))
print(Date.is_date_valid('40-12-2077'))
