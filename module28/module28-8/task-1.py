import os


class File:
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.file = None
        self.list = list()

    def __enter__(self):
        for i_elem in os.listdir(os.getcwd()):
            self.list.append(i_elem)

        if self.filename in self.list:
            print('Файл есть открываю')
            self.mode = 'r'
            self.file = open(self.filename, self.mode, encoding='utf8')
        else:
            print(f'Файла нет создаю новый {self.filename}')
            self.mode = 'w'
            self.file = open(self.filename, self.mode, encoding='utf8')
            self.file.write("Всем привет!")
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()
        return True


with File('example.txt', "r") as file:
    # file.write('dddddddddd')
    print(file.read())
