from abc import ABC, abstractmethod


class Figure:

    def __init__(self, pos_x, pos_y, length, width):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.length = length
        self.width = width

    def move(self, pos_x, pos_y):
        self.pos_x = pos_x
        self.pos_y = pos_y

    def __str__(self):
        return f'x={self.pos_x} y={self.pos_y} длинна={self.length} высота={self.width}\n'


class ResizeAbleMixin:
    def resize(self, length, width):
        self.length = length
        self.width = width
        self.size = length


class Rectangle(Figure, ResizeAbleMixin):
    pass


class Square(Figure, ResizeAbleMixin):

    def __init__(self, pos_x, pos_y, size):
        super().__init__(pos_x, pos_y, size, size)
        self.size = size

    def __str__(self):
        return f'x={self.pos_x} y={self.pos_y} квадрат={self.size} на {self.size}'


rectangle = Rectangle(1, 1, 10, 15)
square = Square(5, 5, 40)
print(rectangle, square)

for i in [rectangle, square]:
    new_size_x = i.length * 6
    new_size_y = i.width * 2
    i.resize(new_size_x, new_size_y)

print('-----------------------------------')
print(rectangle, square)

