original_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(f"Оригинальный список: { original_list } ")

result = list(zip(original_list[::2], original_list[1::2]))

print(f"Новый список: { result } ")

# original_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# print('Оригинальный список:', original_list)
#
# zip_list = zip(original_list[::2], original_list[1::2])
# new_list = []
# for i in zip_list:
#     new_list.append(i)
# print('Новый список:', new_list)

# s = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# print([*map(tuple, zip(s[::2], s[1::2]))])
