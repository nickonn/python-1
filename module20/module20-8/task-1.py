students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology, swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}


def f(dict):
    lst = []
    string = ''
    for i in dict:
        lst += (dict[i]['interests'])
        string += dict[i]['surname']
    return lst, string


pairs = [(i, students[i]['age']) for i in students]
print('Список пар "ID студента — возраст"', pairs)

print('Полный список интересов всех студентов:', set(f(students)[0]),
      '\nОбщая длина всех фамилий студентов:', len(f(students)[1]))
