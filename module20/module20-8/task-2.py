
# text = 'О Дивный Новый мир!'
# # text1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

def crypto(c_list):
    return [v for i, v in enumerate(c_list) if is_prime(i)]

def is_prime(n):
    if n == 2 or n == 3:
        return True
    if n % 2 == 0 or n < 2:
        return False
    for i in range(3, int(n ** 0.5) + 1, 2):
        if n % i == 0:
            return False
    return True

print(crypto([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
print(crypto('О Дивный Новый мир!'))

