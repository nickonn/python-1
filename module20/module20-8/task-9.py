number_rows = int(input('Общее количество строк протокола: '))

score_table = {}

for i in range(number_rows):

    print(i+1, '-я запись:')
    scores, name = input().split()
    scores =int(scores)

    if name not in score_table:
        score_table[name] = scores
    print('Текущий протокол:', score_table)

print('Итоги соревнований:')

sorted_dict = {}
sorted_key = sorted(score_table, key=score_table.get, reverse=True)
for i in sorted_key:
    sorted_dict[i] = score_table[i]

print('Отсортированный список участников', sorted_dict)
count = 0
for i, v in sorted_dict.items():
    count += 1
    if count < 4:
        print(count, '-е место:', i, v)





