def myzip(a, b):
    return ((a[i], b[i]) for i in range(min(len(a), len(b))))


g = myzip("abcd", (10, 20, 30, 40, 50))

print(g)
#print(*g, sep='\n')
for i in g:
    print(i)