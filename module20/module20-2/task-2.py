def cilindr(r, h):

     # Площадь боковой поверхности (r — радиус, h — высота):
    side = (2 * 3.14) * r * h
     # Полная площадь (S — площадь круга):
    S = 3.14 * r ** 2
    full = side + 2 * S
    return side, full

radius = int(input('Введите радиус: '))
height = int(input('Введите высоту: '))

res = cilindr(radius, height )
print(res)
print('Площадь боковой поверхности ', res[0])
print('Полная площадь ', res[1])
