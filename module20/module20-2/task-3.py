import random

def change(nums):

    index = random.randint(0, 5)
    value = random.randint(100, 1000)
    list_new = list(nums)
    list_new[index] = value
    nums = tuple(list_new)
    return nums, value

my_nums = 1, 2, 3, 4, 5
new_nums, rand_val = change(my_nums)
print(new_nums,'Случайное значение', rand_val)

new_nums = change(new_nums)
print(new_nums[0],'Случайное значение', new_nums[1])
rand_val += new_nums[1]
print('Сумма случайных значений:', rand_val)