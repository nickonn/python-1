phone_book = {
    ('Петров', 'Ваня'): 851551555,
    ('Егоров', 'Петя'): 851551555,
}

while True:
    name = input('Фамилию Имя контакта: ').split()
    name = tuple(name)

    if len(name) != 2:
        print('Учись читать')
        break

    if name not in phone_book:
         phone_book[name] = int(input('Введите номер телефона: '))
         print(phone_book)
    else:
        print('Такой контакт уже есть')
        print(phone_book)
