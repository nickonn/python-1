import random
import string

list_one = [random.choice(string.ascii_lowercase) for i in range(10)]
list_two = [random.choice(string.ascii_lowercase) for i in range(10)]

dict_one = dict()
dict_two = dict()

for i, n in enumerate(list_one):
    dict_one[i] = n

for i, n in enumerate(list_two):
    dict_two[i] = n

print(dict_one)
print(dict_two)