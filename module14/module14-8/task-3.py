
def first(fi):
    s = list(str(fi))
    s = list(map(int, s))
    return sum(s)


def second(se):
    return len(str(se))


a = int(input('Ведите число 1: '))
first_func = first(a)
second_func = second(a)

print('Сумма цифр:', first_func)
print('Кол-во цифр в числе:', second_func)
print('Разность суммы и кол-ва цифр:', first_func - second_func)
