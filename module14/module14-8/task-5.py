def func(x):
    count = 1
    for i in range(1, x + 1):
        if x % i == 0:
            count = i
        if count > 1:
            break
    return count


number = int(input('Введите число: '))
print('Наименьший делитель, отличный от единицы:', func(number))
