sum_sym = 0
count = 0
new_list = []

try:
    file_read = open('words.txt', 'r', encoding='utf-8')
    data = file_read.read().split()

    for i in data:
        count += 1
        if i.isalpha():
            print('Слово является палиндромом' if i == i[::-1] else 'Слово не является палиндромом')
        if i.isdigit():
            raise ValueError('Обнаруженны цифры в строке {}'.format(count))
    file_read.close()

except FileNotFoundError:
    print("Файл не найден")
except ValueError:
    print('Записываю в лог строку с цифрами')
    file_write = open('errors.log', 'w', encoding='utf-8')
    file_write.write("строка: " + str(count) + " Содержит цифры: " + i)
    file_write.close()
    raise








