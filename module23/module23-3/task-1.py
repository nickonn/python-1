
try:
    text = int(input("Введите цифры: "))
except ValueError:
    print("Неверный тип данных")
else:
    print("Все хорошо")
finally:
    text = input("Введите строку: ")

file_name = 'test.txt'
try:
    file_write = open(file_name, 'x', encoding='utf-8')
    file_write.write(text)
    file_write.close()
except FileExistsError:
    print('Файл', file_name, 'уже существует: записать: y/n')
    name = input()
    if name == 'y':
        file_write = open(file_name, 'w', encoding='utf-8')
        file_write.write(text)
        file_write.close()
    else:
        print("Не записано")
except TypeError:
    print("Должна быть строка")


