def power(a, n):
    if n == 1:
        return a
    if n != 1:
        return a * power(a, n-1)

try:
    float_num = float(input('Введите вещественное число: '))
except ValueError:
    try:
        print("Это не вещественное число")
        float_num = float(input('Введите вещественное число еще раз: '))
    finally:
        print("Тупой")

try:
    int_num = int(input('Введите степень числа: '))
except ValueError:
    print("Это не целое число")
except NameError:
    print("Вы не ввели степень")
else:
    print(float_num, '**', int_num, '=', power(float_num, int_num))







