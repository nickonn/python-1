import string

data_list = list()
count_line = 0
with open('registrations.txt', 'r', encoding='utf-8') as file_read:
    for i in file_read:
        data_list.append(i.split())

    for k in data_list:
        count_line += 1
        x = chr(64)
        # print(int(k[2]))
        try:
            if len(k) < 3:
                raise IndexError

            if not k[0].isalpha():
                raise NameError

            if k[1].find('@') == -1 or k[1].find('.') == -1 :
                raise SyntaxError

            if int(k[2]) < 10 or int(k[2]) > 99:
                raise ValueError

        except IndexError:
            print('в строке:', count_line, 'НЕ присутствуют все три поля:', k)
        except NameError:
            print('в строке:', count_line, 'Поле имени содержит НЕ только буквы:', k)
        except SyntaxError:
            print('в строке:', count_line, 'Поле «Имейл» НЕ содержит @ и .(точку):', k)
        except ValueError:
            print('в строке:', count_line, 'Поле «Возраст» НЕ является числом от 10 до 99:', k)

