sum_sym = 0
count = 0
with open('people.txt', 'r', encoding='utf-8') as file_read:
    try:
        for i in file_read:
            try:
                lenght = len(i)
                count += 1
                if i.endswith("\n"):
                    lenght -= 1
                if lenght < 3:
                    raise BaseException
                sum_sym += lenght
            except BaseException:
                print('Ошибка: менее трёх символов в строке: {}'.format(count))
                with open('errors.log', 'a', encoding='utf-8' ) as error_file:
                    error_file.write(i)
                # raise
    except FileNotFoundError:
        print("Файл не найден")
    finally:
        print("Сумма:", sum_sym)
        with open('errors.log', 'a', encoding='utf-8') as error_file:
            error_file.write('\n')