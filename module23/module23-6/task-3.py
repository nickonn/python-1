import random

numbers_list = []
work = True

def do_it():
    sum = 0
    number = int(input("Введите число: "))

    if (len(numbers_list) > 0):
        n = random.randint(3, 13)

        try:
            if n == 13:
                raise BaseException
        except BaseException:
            print('Вас постигла неудача!')
            with open('out_file.txt', 'w', encoding='utf-8') as file_write:
                for i in numbers_list:
                    file_write.write(str(i))
                    file_write.write('\n')
            exit()

        numbers_list.append(number)
        with open('out_file.txt', 'w', encoding='utf-8') as file_write:
            for i in numbers_list:
                file_write.write(str(i))
                file_write.write('\n')

        for i in range(0, len(numbers_list)):
            sum += numbers_list[i];
            if sum >= 777:
                print('Вы успешно выполнили условие для выхода из порочного цикла!')
                # work = False
                exit()
    else:
        numbers_list.append(number)

while (work):
    do_it()
