import os
import string

file_read = open('ages.txt', 'r', encoding='utf-8')

str_name = ('Vasia', 'Petia', 'Kolia', 'Dima', 'Sasa', 'ddd')
data = file_read.read().split()
file_read.close()

data_dict = dict(zip(str_name, data))

try:
    file_name = 'result.txt'
    abs_patch = os.path.abspath(file_name)
    print(abs_patch)

    file_write = open(abs_patch, 'x', encoding='utf-8')
    for k, v in data_dict.items():
        str_data = k + " " + v
        file_write.write(str_data)
        file_write.write('\n')
    file_write.close()
except FileExistsError:
    print('Файл', file_name, 'уже существует')
except IsADirectoryError:
    print('Оказалась директория')
except ValueError:
    print('Неверный тип данных')





