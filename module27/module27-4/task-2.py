import time
from typing import Callable, Any

PLUGINS = dict()


def register(func: Callable) -> Callable:
    """Декоратор. Регистрирует функцию как плагин"""
    PLUGINS[func.__name__] = func
    return func

@register
def say_hello(name: str) -> str:
    return f'Hello {name}!'

@register
def say_goodbye(name: str) -> str:
    return f'Goodbye {name}!'


print(PLUGINS)
print(say_hello('Tom'))