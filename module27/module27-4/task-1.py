
def bread(func):
    def wrapper(*args, **kwargs):
        print("</----------\>")
        func(*args, **kwargs)
        print("<\______/>")
    return wrapper

def ingridient(func):
    def wrapper_in(*args, **kwargs):
        print('#помидоры#')
        func(*args, **kwargs)
        print('~салат~')
    return wrapper_in

def nachinka(name='--ветчина--'):
    print(f'{name}')

@bread
@ingridient
def sandwich():
    nachinka()


sandwich()


@bread
@ingridient
def filling_burger(filler):
    print(filler)


filling_burger("ветчина")

#--------------------------------------------

plugins = {}

def go_to_plugins(func):
    plugins[func.__name__] = func
    return func


@go_to_plugins
def hello():
    print('Hello!')

print(plugins)