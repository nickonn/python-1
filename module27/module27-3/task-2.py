import time
from typing import Callable, Any


def timer(func: Callable) -> Callable:
    """ Декоратор, выводящий время работы декорируемой функции """

    def wrapped_func(*args, **kwargs) -> Any:
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        res_time = round(end - start, 4)
        print(f'Функция работала {res_time} секунд')
        return result
    return wrapped_func

@timer
def squres_sum(num: int):
    result = 0
    for _ in range(num + 1):
        result += sum([i ** 2 for i in range(10000)])
    return result


@timer
def squres_sum1(num: int):
    result = 0
    for _ in range(num + 1):
        result += sum([i ** 3 for i in range(10000)])
    return result

# squres_sum(500)
my_dec = squres_sum(500)
print(my_dec)

my_dec1 = squres_sum1(500)
print(my_dec1)