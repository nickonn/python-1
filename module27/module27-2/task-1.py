def func_1(x):
    return x + 10


def func_2(func, *args):
    return func(*args) * func(*args)


print(func_2(func_1, 9))
