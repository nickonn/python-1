import time
from typing import Callable, Any


def squres_sum(num: int):
    result = 0
    for _ in range(num + 1):
        result += sum([i ** 2 for i in range(10000)])
    return result


def timer(func: Callable, *args, **kwargs) -> Any:
    """ Фунция-таймер. Выводит время работы функции и возвращает ее результат """

    start = time.time()
    result = func(*args, **kwargs)
    end = time.time()
    res_time = round(end - start, 4)
    print(f'Функция работала {res_time} секунд')
    return result


my = timer(squres_sum, 500)
print(my)