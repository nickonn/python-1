import time


def sleep(func):
    def wrapper():
        print('The time is start:', time.ctime())
        func()
        time.sleep(5)
        print('The time is end:', time.ctime())
    return wrapper


@sleep
def test():
    print('<Тут что-то происходит...>')

@sleep
def test1():
    print('<Тут что-то происходит1...>')

test()
print('-------------------')
test1()


# import time
#
# def sleep(func):
#
#     print('The time is start:', time.ctime())
#     time.sleep(5)
#     print('The time is end:', time.ctime())
#     return func
#
#
# @sleep
# def test():
#     print('<Тут что-то происходит...>')
#
# @sleep
# def test1():
#     print('<Тут что-то происходит1...>')
#
# test()
# print('-------------------')
# test1()