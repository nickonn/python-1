
def how_are_you(func):
    def wrapper():
        text = input("Как дела? ")
        print(f'А у меня не очень! Ладно, держи свою функцию. {func.__name__}')
        func()
    return wrapper


@how_are_you
def test():
    print('<Тут что-то происходит...>')

@how_are_you
def test1():
    print('<Тут что-то происходит1...>')


test()
test1()