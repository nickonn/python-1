import functools


def debug(func):

    @functools.wraps(func)
    def wrapper(*args, **kwargs):

        str_arg_kwarg = ''
        for arg in args:
            if isinstance(arg, str):
                str_arg_kwarg += ''.join([f"'{arg}'"])
            else:
                str_arg_kwarg += str(arg)

        for key, value in kwargs.items():
            if isinstance(value, str):
                str_arg_kwarg += f"{key}='{value}'"
            else:
                str_arg_kwarg += f', {key}={value}'

        print('Вызывается', func.__name__, f' ({str_arg_kwarg})')
        res = func(*args, **kwargs)
        print(repr(func.__name__), 'вернула значение', f' {repr(res)}\n', f'{res}\n')

    return wrapper


@debug
def greeting(name, age=None):
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


greeting("Том")
greeting("Миша", age=100)
greeting(name="Катя", age=16)
