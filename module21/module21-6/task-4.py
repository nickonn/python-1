site = {
    'html': {
        'head': {
            'title': 'Мой сайт'
        },
        'body': {
            'h2': 'Здесь будет мой заголовок',
            'div': 'Тут, наверное, какой-то блок',
            'p': 'А вот здесь новый абзац'
        }
    }
}


def find_key(struct, key, num=None):
    if key in struct:
        return struct[key]

    if num > 1:
        for i in struct.values():
            if isinstance(i, dict):
                result = find_key(i, key, num - 1)
                if result:
                    break

    else:
        result = None
    return result

user_key = input("Введите искомый ключ: ")

glub = input("Хотите ввести максимальную глубину? Y/N: ").lower()
if glub == "y":
    number = int(input("Ввелите максимальную глубину: "))
else:
    number = 0

value = find_key(site, user_key, number)

if value:
    print(value)
else:
    print("Такого ключа в структуре нет")