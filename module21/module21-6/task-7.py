from typing import Iterable
# def summ(*args):
#     def flatten(a_list):
#         result = []
#         for e in a_list:
#            if isinstance(e, int):
#                result.append(e)
#            else:
#                result.extend(flatten(e))
#         return result
#     return sum(flatten(args))

def sum_(*args):
    return sum(sum_(*arg) if isinstance(arg, Iterable) else arg for arg in args)

print(sum_(1, 2, 3, 4, 5))
print(sum_([[1, 2, [3]], [1], 3]))