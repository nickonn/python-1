def myzip(*args):
    min_len = min(len(elem) for elem in args)
    tuple_list = [tuple(struct[i] for struct in map(list, args)) for i in range(min_len)]
    return tuple_list

# def myzip(*args):
#     length = min((len(elem) for elem in args))
#     f_lst = [list(elem) for elem in args]
#     snd_lst = list(tuple(elem[i] for elem in f_lst) for i in range(length))
#     return snd_lst

# def myzip(*args: iter) -> tuple:
#     count = 0
#     args = list(map(iter, args))
#
#     while True:
#         try:
#             yield tuple(next(i) for i in args)
#             count += 1
#         except RuntimeError:
#             break

# a = [{'x': 4}, 'b', 'z', 'd']
# b = (10, {20,}, [30], 'z')
a = [1, 2, 3, 4, 5]
b = {1: 's', 2: 'q', 3: 4}
x = (1, 2, 3, 4, 5)

print(myzip( a, b, x))