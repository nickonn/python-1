# def calculating_math_func(data):
#     result = 1
#     for index in range(1, data + 1):
#         result *= index
#     result /= data ** 3
#     result = result ** 10
#     return result


def calculating_math_func(data):
    if data in factorials:  # Если уже вычислялось
        result = factorials[data]
    else:
        result = max(factorials.values())  # Берем самое последнее вычисленное и начинаем отсюда
        for index in range(max(factorials.keys()) + 1, data + 1):
            result *= index
            factorials[index] = result  # Заполняем словарь всем, что вычислили
    result /= data ** 3
    result = result ** 10
    return result


factorials = {1: 1}
while True:
    print(calculating_math_func(int(input('Введите число: '))))
    print(sorted(factorials.items()))  # Проверка самого себя