string = input('Введите данные: ')

def get_type(self):
    ru_types = {
        int: 'int (целое число)',
        float: 'float (вещественное число)',
        str: 'str (строка)',
        tuple: 'tuple (кортеж)',
        bool: 'bool (логический тип)',
        list: 'list (список)',
        dict: 'dict (словарь)',
        set: 'set множество',
    }
    return ru_types.get(type(eval(self)), 'Неизвестный тип')


if isinstance(string, (int, float, str, tuple, bool)):
    try:
        print('Тип данных', get_type(string))
    except NameError:
        print('Строки должны быть в кавычках "" или одинарных')
    print('Неизменяемый (immutable)')

elif isinstance(string, (list, dict, set)):
    try:
        print('Тип данных', get_type(string))
    except NameError:
        print('Строки должны быть в кавычках "" или одинарных')
    print('Изменяемый (mutable)')

print('ID объекта', id(string))



