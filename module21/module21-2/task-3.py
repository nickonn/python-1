site = {
    'html': {
        'head': {
            'title': 'Мой сайт'
        },

        'body': {
            'h2': 'Здесь будет мой заголовок',
            'div': 'Тут, наверное, какой-то блок',
            'p': 'А вот здесь новый абзац'
        }
    }
}

def find_key(struct, key):
    if key in struct:
        return struct[key]

    for i in struct.values():
        if isinstance(i, dict):
            result = find_key(i, key)
            if result:
                break
    else:
        result = None
    return result


user_key = input("Введите какой ключ ищем: ")
value = find_key(site, user_key)
if value:
    print(value)
else:
    print("Такого ключа нет в структуре сайта")

