def ask_user(question,
             compilant='Неверный ввод. Пожалуйста, введите да или нет',
             retries=3):
    while True:
        answer = input(question).lower()
        if answer == 'да':
            return 1
        if answer == 'нет':
            return 0
        retries -= 1
        if retries == 0:
            print("Количество попыток истекло")
            break
        print(compilant)
        print("Осталось попыток", retries)

ask_user('Вы действительно хотите выйти?')

ask_user('Удалить файл?', 'Так да или нет')

ask_user('Записать файл?', retries=4)