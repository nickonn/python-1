def create_dict(data):
    if isinstance(data, dict):
        return data

    if isinstance(data, int) or isinstance(data, float) or isinstance(data, str):
        return {data: data}


def data_preparation(old_list):
    new_list = []
    for i_element in old_list:
        new_list.append(create_dict(i_element))
    return new_list

# def data_preparation(old_list):
#     result = []
#     for item in old_list:
#         if isinstance(item, dict):
#             result.append(item)
#         elif isinstance(item, (str, int, float)):
#             result.append({item: item})
#     return result


data = ["sad", {"sds": 23}, {43}, [12, 42, 1], 2323]
data = data_preparation(data)

print(data)

