message = []
decode = []
lost_message = 0

packet = int(input('Кол-во пакетов: '))

for i in range(packet):
    print('Пакет номер:', i+1)
    for i_bit in range(4):
        print(i_bit + 1, 'бит', end=' ')
        bit = int(input())
        message.append(bit)
    if message.count(-1) <= 1:
        decode.extend(message)
    else:
        print('Много ошибок в тексте')
        lost_message += 1
    message = []

print('Полученное сообщение:', decode)
print('Кол-во ошибок в сообщении:', decode.count(-1))
print('Кол-во потерянных пакетов:', lost_message)
