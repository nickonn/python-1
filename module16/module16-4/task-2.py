list_group = []

group = int(input('Кол-во участников: '))
people = int(input('Кол-во человек в команде: '))

num = 1

if group % people != 0:
    print(group, 'участников невозможно поделить на команды по', people, 'человек')
else:
    for i in range(1, group // people + 1):
        list_group.append(list(range(num, people+num)))
        num += people
    print(list_group)