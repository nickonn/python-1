def is_film_true(name_film, list_film):
    for i in list_film:
        if name_film == i:
            return True
    return False

films = [
    'Крепкий орешек', 'Назад в будущее', 'Таксист',
    'Леон', 'Богемская рапсодия', 'Город грехов',
    'Мементо', 'Отступники', 'Деревня',
    'Проклятый остров', 'Начало', 'Матрица'
]
new_list = []


while True:
    print('Ваш текущий топ фильмов:', new_list)
    my_film = input('Название фильма: ')

    if is_film_true(my_film, films):
        print('\nКоманды: добавить, вставить, удалить')
        com = input('Введите команду: ')

        if com == 'добавить':
            new_list.append(my_film)

        if com == 'удалить':
            if is_film_true(my_film, new_list):
                new_list.remove(my_film)
            else:
                print('Такого фильма нет в нашем списке')

        if com == 'вставить':
            index = int(input('На какое место: '))
            new_list.insert(index-1, my_film)

    else:
        print('Такого фильма нет в списке')

