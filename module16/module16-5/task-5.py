violator_songs = [
    ['World in My Eyes', 4.86],
    ['Sweetest Perfection', 4.43],
    ['Personal Jesus', 4.56],
    ['Halo', 4.9],
    ['Waiting for the Night', 6.07],
    ['Enjoy the Silence', 4.20],
    ['Policy of Truth', 4.76],
    ['Blue Dress', 4.29],
    ['Clean', 5.83]
]

kol = int(input('Сколько песен выбрать? '))
count = 0
drop = 0
for i in range(kol):
    print('Название', i+1, 'песни: ')
    name_song = input()

    for i in violator_songs:
        if i[0] == name_song:
           count += i[1]
           drop += 1
        else:
            drop = 0
    if drop == 0:
        print('Такой песни нет в списке')

print('Общее время звучания песен:', round(count,2), 'минуты')