first_list = []
second_list = []
new_list = []

first_ch = 0
second_ch = 0

for i in range(3):
    print("Введите", i+1, 'число для первого списка')
    first_ch = input()
    first_list.append(first_ch)

for i in range(7):
    print("Введите", i+1, 'число для второго списка')
    second_ch = input()
    second_list.append(second_ch)

new_list.extend(first_list)
new_list.extend(second_list)
unique_list = set(new_list)

print('Первый список:', first_list)
print('Второй список:', second_list)

print('Новый первый список с уникальными элементами:', unique_list)